package com.company.lesson10_modyficators_equals_interfaces.equals;

public class Main {
    public static void main(String[] args) {
        Person igor = new Person("Igor", 20);
        Person igor2 = new Person("Igor", 20);

        System.out.println(igor == igor2);
        System.out.println(igor);
        System.out.println(igor2);

        String statStr = "hello";
        String dynamicStr = new String("hello");

        System.out.println(statStr == dynamicStr);
        System.out.println(statStr.equals(dynamicStr));

        Object object = new Person("sdf", 20);

        Person igor3 = igor;

        System.out.println(igor.equals(igor2));

        System.out.println(igor.hashCode());
        System.out.println(igor2.hashCode());

        igor3.setName("Andriy");

        System.out.println(igor);
        System.out.println(igor3);
    }
}
