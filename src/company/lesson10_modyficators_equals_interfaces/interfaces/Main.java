package com.company.lesson10_modyficators_equals_interfaces.interfaces;

public class Main {
    public static void main(String[] args) {
        Person person = new Person("Igor", 20);

        person.goToWork();

        Workable workable = new Person("Nazar", 25);

        Workable.m();

//        Workable.work = "new work";

    }
}
