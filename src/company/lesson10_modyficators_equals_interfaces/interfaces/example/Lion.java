package com.company.lesson10_modyficators_equals_interfaces.interfaces.example;

import java.util.Objects;

public class Lion extends Animal{

    private double speedOfRun;

    public Lion(String name, int weight, double speedOfRun) {
        super(name, weight);
        this.speedOfRun = speedOfRun;
    }

    @Override
    public void eat() {
        System.out.println("I'm lion and I eat meet.");
    }

    public double getSpeedOfRun() {
        return speedOfRun;
    }

    public void setSpeedOfRun(double speedOfRun) {
        this.speedOfRun = speedOfRun;
    }

    @Override
    public String toString() {
        return super.toString() + " Lion{" +
                "speedOfRun=" + speedOfRun +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Lion lion = (Lion) o;
        return Double.compare(lion.speedOfRun, speedOfRun) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), speedOfRun);
    }
}
