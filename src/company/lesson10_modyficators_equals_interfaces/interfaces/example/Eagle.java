package com.company.lesson10_modyficators_equals_interfaces.interfaces.example;

import java.util.Objects;

public class Eagle extends Animal implements Flyable {

    private double speedOfFlying;

    public Eagle(String name, int weight, double speedOfFlying) {
        super(name, weight);
        this.speedOfFlying = speedOfFlying;
    }

    @Override
    public void fly() {
        System.out.println("I'm flying");
    }

    @Override
    public void eat() {
        System.out.println("I'm eagle and I eat mouse.");
    }

    public double getSpeedOfFlying() {
        return speedOfFlying;
    }

    public void setSpeedOfFlying(double speedOfFlying) {
        this.speedOfFlying = speedOfFlying;
    }

    @Override
    public String toString() {
        return "Eagle{" +
                "speedOfFlying=" + speedOfFlying +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Eagle eagle = (Eagle) o;
        return Double.compare(eagle.speedOfFlying, speedOfFlying) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), speedOfFlying);
    }
}
