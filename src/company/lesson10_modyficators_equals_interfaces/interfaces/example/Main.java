package com.company.lesson10_modyficators_equals_interfaces.interfaces.example;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Eagle eagle = new Eagle("Black", 20, 100);

        List<Flyable> eagles = new ArrayList<>();
        eagles.add(eagle);
        eagles.add(new Eagle("White", 25, 110));
//        eagles.add(new Lion("Simba", 80, 60));

        for (Flyable flyable : eagles) {
            flyable.fly();
        }
    }
}
