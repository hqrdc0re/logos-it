package com.company.lesson10_modyficators_equals_interfaces.interfaces;

public interface Workable {

    String work = "String";

    void goToWork();

    default void getSalary() {
        System.out.println("Uraaa, I've go salary!");
    }

    static void m() {
        System.out.println("m");
    }

}
