package com.company.lesson10_modyficators_equals_interfaces.interfaces.example2;

public interface Printer {
    void print(String text);
}
