package com.company.lesson10_modyficators_equals_interfaces.interfaces.example2;

public class FilePrinter implements Printer{
    @Override
    public void print(String text) {
        System.out.println("Print in file: " + text);
    }
}
