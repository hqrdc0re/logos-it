package com.company.lesson10_modyficators_equals_interfaces.interfaces.example2;

public class ConsolePrinter implements Printer{

    @Override
    public void print(String text) {
        System.out.println(text);
    }
}
