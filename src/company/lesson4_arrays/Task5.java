package com.company.lesson4_arrays;

public class Task5 {
    public static void main(String[] args) {
        int[] array1 = {10, 15, 5, -4};
        int[] array2 = {5, 11, 10, -10};

        int n = 4;

        int[] array3 = new int[n];

        for (int i = 0; i < n; ++i) {
            array3[i] = array1[i] - array2[i];
        }

        for (int i = 0; i < n; ++i) {
            System.out.println(array3[i]);
        }
    }
}
