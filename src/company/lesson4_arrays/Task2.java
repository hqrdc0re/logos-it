package com.company.lesson4_arrays;

public class Task2 {
    public static void main(String[] args) {
        int[] array = {0, 10, 5, 0, 11, 4, 10, 6, 4, 10, 0, 6, -2, 10};

        int[] dublicates = new int[1000];
        int size = 0;

        for (int i = 0; i < array.length; ++i) {

            if (contains(dublicates, array[i], size)) {
                continue;
            }

            for (int j = i + 1; j < array.length; ++j) {
                if (array[i] == array[j]) {
                    dublicates[size] = array[i];
                    ++size;
                    break;
                }
            }
        }

        for (int i = 0; i < size; ++i) {
            System.out.println(dublicates[i]);
        }
    }

    public static boolean contains (int[] array, int value, int size){
        for (int i = 0; i < size; ++i) {
            if (array[i] == value) {
                return true;
            }
        }

        return false;
    }
}
