package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class HW5_Malkevych_Oleh {

    static Random random = new Random();
    private static final String[] words = {"bank", "weather", "night", "arizona", "laptop", "cycle"};
    private static String userWord = "";
    private static final String secretWord = words[random.nextInt(words.length)];


    public static void main(String[] args) {
        printWelcome();
        generateSecretWord();
        startGame();
    }
    public static void printWelcome (){

        System.out.println("Hello!");
        System.out.println("Welcome to hangman!");
        System.out.println("You must guess a word!");
        System.out.println("You can write letter or whole word!");
        System.out.println("Good luck! Have fun!");
        System.out.println();
        System.out.println("-----------------------");
        System.out.println();
        System.out.println("Required word");
    }

    public static void generateSecretWord(){

        userWord = "_".repeat(secretWord.length());
//        for (int i = 0; i <secretWord.length() ; i++) {
//            userWord += "_";
//        }
        System.out.println(userWord);
        System.out.println(secretWord);
    }

    public static void startGame( ){
        Scanner scan = new Scanner(System.in);

        int numberOfTries = 5;
        int n = 0;
        int countGuessWord = 0;
        String letter = "";

        // Ліст підчеркувань секретного слова напр (bank - ____) , щоб в подальшому заміняти і виводити вгадані букви
        List <String> listWithUndefinedWord = new ArrayList<>();
        for (int i = 0; i < secretWord.length(); i++) {
            listWithUndefinedWord.add("_");
        }
        //Створив ліст для секретного слова щоб розпреділити його на букви , щоб в подальшому перевіряти введене слово
        List <String> listSecretWord = new ArrayList<>();
        //Заповнюю ліст
        for (int i = 0; i < secretWord.length(); i++) {
            listSecretWord.add(secretWord.substring(i, i + 1));
        }
        boolean whileIsTrue = true;
        while (whileIsTrue){
            System.out.print("\nВведіть букву : ");
            letter = scan.nextLine();
            for (int i = 0; i < secretWord.length(); i++) {
                if (listSecretWord.get(i).equals(letter)) {
                    listWithUndefinedWord.set(i , letter);
                    countGuessWord++;
                }else{
                    n++;
                }
            }
            if (n == secretWord.length()){
                numberOfTries--;
            }
            n = 0;

            System.out.print(listWithUndefinedWord);
            System.out.println("\nКількість спроб " + numberOfTries);

            if (countGuessWord == secretWord.length()) {
                whileIsTrue = false;
                printEndIfWin();
            }else if(numberOfTries == 0){
                whileIsTrue = false;
                printEndIfLose();
            }

        }

    }
    public static void printEndIfWin(){
        System.out.println("\nYou win!!! =) " +
                "\nIf you want play again , push the start button");
    }
    public static void printEndIfLose(){
        System.out.println("\nYou lose =( " +
                "\nIf you want play again , push the start button");
    }
}

