package com.company.lesson1_types_mathOperations_if;

public class HW1_Surname_Name {
    public static void main(String[] args) {
        double a = 5;
        double b = 10;
        double c = 3;

        double d = b * b - 4 * a * c;

        if (d > 0) {
            double x1 = (-b - Math.sqrt(d)) / (2 * a);
            double x2 = (-b + Math.sqrt(d)) / (2 * a);
            System.out.println(x1);
            System.out.println(x2);
        } else if (d == 0) {
            double x = -b / (2 * a);
            System.out.println(x);
        } else {
            System.out.println("Коренів немає");
        }

        int x = 10;
        int y;

        if (x < 10) {
            y = x + 9;
        } else if (x > 15) {
            y = x - 5;
        } else {
            y = x * x;
        }

        System.out.println(y);

        double a1 = 10;
        double b1 = 5;
        double c1 = 11;
        double d1 = 6;

        double res = a1 + b1 * (c1 - d1) / 2;

        System.out.println(res);
    }
}
