package com.company;


import java.awt.datatransfer.StringSelection;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        String str = "device";

        List<String> letters = str.chars().mapToObj(c -> String.valueOf((char) c))
                .collect(Collectors.toList());

        System.out.println(letters);

        System.out.println(str.substring(5, 6));
    }
}
