package company.lesson11_SOLID.mywrite.lesson11_SOLID.liskov_substitution;

public abstract class Animal {

    public abstract void eat();
}
