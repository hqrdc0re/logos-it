package company.lesson11_SOLID.mywrite.lesson11_SOLID.liskov_substitution;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List <Animal> animals = new ArrayList<>();

        animals.add(new Lion());
        animals.add(new Mouse());

        for (Animal animal : animals) {
            animal.eat();
        }
    }
}
