package company.lesson11_SOLID.mywrite.lesson11_SOLID.liskov_substitution;

public class Lion extends Animal{
    @Override
    public void eat() {
        System.out.println("I eat meat");
    }
}
