package company.lesson11_SOLID.mywrite.lesson11_SOLID.single_responsibylity;

public class Auto {

    private String brand;
    private double price;

    public void run() {
        System.out.println("I run");
    }

//    public void sellCar() { Лишній функціонал, який не притаманний сутності машина
//
//    }
}
