package company.lesson11_SOLID.mywrite.lesson11_SOLID.interface_segregation.bad_practise;

public interface Printer {
    void printInConsole(String text);
    void printInFile(String text);
}
