package company.lesson11_SOLID.mywrite.lesson11_SOLID.interface_segregation.bad_practise;

public class FilePrinter implements Printer{

    @Override
    public void printInConsole(String text) {

    }

    @Override
    public void printInFile(String text) {
        System.out.println("Print in file: " + text);
    }
}
