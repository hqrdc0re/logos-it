package company.lesson11_SOLID.mywrite.lesson11_SOLID.interface_segregation.fixed_with_classes;

public class ConsolePrinter implements Printer{
    @Override
    public void print(String text) {
        System.out.println("Print in console: " + text);
    }
}
