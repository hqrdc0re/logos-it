package company.lesson11_SOLID.mywrite.lesson11_SOLID.interface_segregation.fixed_with_classes;

public interface Printer {
    void print(String text);
}
