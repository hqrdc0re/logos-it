package company.lesson11_SOLID.mywrite.lesson11_SOLID.interface_segregation.fixed_with_classes;

public class FilePrinter implements Printer{
    @Override
    public void print(String text) {
        System.out.println("Print in file: " + text);
    }
}
