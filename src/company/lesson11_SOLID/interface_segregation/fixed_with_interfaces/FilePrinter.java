package company.lesson11_SOLID.mywrite.lesson11_SOLID.interface_segregation.fixed_with_interfaces;

public interface FilePrinter {

    void printIfFile(String text);
}
