package company.lesson11_SOLID.mywrite.lesson11_SOLID.interface_segregation.fixed_with_interfaces;

public class Printer implements ConsolePrinter{
    @Override
    public void printInConsole(String text) {
        System.out.println("Print in console: " + text);
    }
}
