package company.lesson11_SOLID.mywrite.lesson11_SOLID.open_closed;

public class Main {
    public static void main(String[] args) {
        Tesla tesla = new Tesla("tesla");
        Audi audi = new Audi();
        Honda honda = new Honda("honda");


        System.out.println(getPrice(honda));
    }

    public static int getPrice(GetPrice auto){
        return auto.getPrice();
    }
}
