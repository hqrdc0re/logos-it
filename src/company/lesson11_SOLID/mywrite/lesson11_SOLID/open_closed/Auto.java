package company.lesson11_SOLID.mywrite.lesson11_SOLID.open_closed;

public class Auto {
    private String brand;

    public Auto(String brand){
            this.brand = brand;
    }

    public String getBrand(){
        return brand;
    }

    public void setBrand(String brand){
        this.brand = brand;
    }

}
