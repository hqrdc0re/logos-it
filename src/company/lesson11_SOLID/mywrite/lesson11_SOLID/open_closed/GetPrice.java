package company.lesson11_SOLID.mywrite.lesson11_SOLID.open_closed;

public interface GetPrice {
        public int getPrice();
}
