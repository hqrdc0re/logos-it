package company.lesson11_SOLID.mywrite.lesson11_SOLID.interface_segregation.fixed_with_interfaces_third;

public class PrinterInFile implements FilePrinter{

    @Override
    public void printInFile(String text) {
        System.out.println("Print in file: " + text);
    }
}
