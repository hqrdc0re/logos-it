package company.lesson11_SOLID.mywrite.lesson11_SOLID.interface_segregation.fixed_with_interfaces_third;

public interface ConsolePrinter {
    void printInConsole(String text);
}
