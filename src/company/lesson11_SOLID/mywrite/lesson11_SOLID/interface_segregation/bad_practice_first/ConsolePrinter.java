package company.lesson11_SOLID.mywrite.lesson11_SOLID.interface_segregation.bad_practice_first;

public class ConsolePrinter implements  Printer{

    @Override
    public void printInConsole(String text) {
        System.out.println("print in console: " + text);
    }

    @Override
    public void printInFile(String text) {

    }
}
