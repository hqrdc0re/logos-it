package company.lesson11_SOLID.mywrite.lesson11_SOLID.interface_segregation.bad_practice_first;

public interface Printer {
    void printInConsole(String text);
    void printInFile(String text);

}
