package company.lesson11_SOLID.mywrite.lesson11_SOLID.interface_segregation.fixed_with_classes_second;

public interface Printer {
    void print(String text);
}
