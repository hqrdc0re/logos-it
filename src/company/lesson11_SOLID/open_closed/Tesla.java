package company.lesson11_SOLID.mywrite.lesson11_SOLID.open_closed;

public class Tesla implements GetPrice{

    public String brand;

    public Tesla(String brand) {
        this.brand = brand;
    }

    public void run() {

    }

    @Override
    public int getPrice() {
        return 80000;
    }
}
