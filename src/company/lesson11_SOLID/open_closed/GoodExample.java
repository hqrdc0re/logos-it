package company.lesson11_SOLID.mywrite.lesson11_SOLID.open_closed;

public class GoodExample {
    public static void main(String[] args) {
        Tesla tesla = new Tesla("Tesla");
        Audi audi = new Audi();

        tesla.run();

        System.out.println(getPrice(tesla));

        Honda honda = new Honda();

        System.out.println(getPrice(honda));
    }

    public static int getPrice(GetPrice auto) {
        return auto.getPrice();
    }
}
