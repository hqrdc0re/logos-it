package company.lesson11_SOLID.mywrite.lesson11_SOLID.open_closed;

import java.util.ArrayList;
import java.util.List;

// Bad example, don't do like this
public class Main {
    public static void main(String[] args) {
        Auto tesla = new Auto("Tesla");
        Auto audi = new Auto("Audi");

        List<Auto> cars = new ArrayList<>();

        cars.add(tesla);
        cars.add(audi);

        System.out.println(getPrice(tesla));

        Auto honda = new Auto("Honda");

    }

    public static int getPrice(Auto auto) {
        switch (auto.getBrand()) {
            case "Tesla": {
                return 80000;
            }
            case "Audi": {
                return 50000;
            }
            case "Honda": {
                return 35000;
            }
            default: {
                return 0;
            }
        }
    }
}
