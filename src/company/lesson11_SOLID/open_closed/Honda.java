package company.lesson11_SOLID.mywrite.lesson11_SOLID.open_closed;

public class Honda implements GetPrice{
    @Override
    public int getPrice() {
        return 35000;
    }
}
