package company.lesson11_SOLID.mywrite.lesson11_SOLID.dependency_injection;

public class RockMusic extends Music{

    @Override
    public void play() {
        System.out.println("Play rock");
    }
}
