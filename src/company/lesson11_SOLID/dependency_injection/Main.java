package company.lesson11_SOLID.mywrite.lesson11_SOLID.dependency_injection;

public class Main {
    public static void main(String[] args) {
        ClassicMusic classicMusic = new ClassicMusic();
        RockMusic rockMusic = new RockMusic();

        MusicPlayer musicPlayer = new MusicPlayer(classicMusic);
    }
}
