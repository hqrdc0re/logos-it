package company.lesson11_SOLID.mywrite.lesson11_SOLID.dependency_injection;

public class MusicPlayer {
    private Music music;

    public MusicPlayer(Music music) {
        this.music = music;
    }

    public void play() {
        music.play();
    }
}
