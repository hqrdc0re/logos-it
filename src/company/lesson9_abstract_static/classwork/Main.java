package com.company.lesson9_abstract_static.classwork;

public class Main {
    public static void main(String[] args) {
        int x = 5;

        Animal animal = new Animal(10);

        int i = 5;
        System.out.println(i);
        changeValue(i);
        System.out.println(i);

        System.out.println(animal);
        changeValue(animal);
        System.out.println(animal);

        Object obj;
    }

    public static void changeValue(int x) {
        x = 10;
    }

    public static void changeValue(Animal animal) {
        animal.setWeight(20);
    }
}
