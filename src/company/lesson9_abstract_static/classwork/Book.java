package com.company.lesson9_abstract_static.classwork;

public class Book extends Product{

    private String title;

    public Book(double price, String title) {
        super(price);
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void sell() {
        System.out.println("Sell");
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                '}';
    }
}
