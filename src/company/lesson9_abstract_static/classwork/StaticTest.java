package com.company.lesson9_abstract_static.classwork;

public class StaticTest {
    public static void main(String[] args) {

        System.out.println(Animal.getNumberOfAnimals());
        Animal.getNumberOfAnimals();

        Animal animal = new Animal(10);
        Animal animal1 = new Animal(20);

        animal.getWeight();

        String str;

        System.out.println(Animal.getNumberOfAnimals());
    }
}
