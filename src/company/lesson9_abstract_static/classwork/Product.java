package com.company.lesson9_abstract_static.classwork;

public abstract class Product {

    private double price;

    public Product(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public abstract void sell();

    @Override
    public String toString() {
        return "Product{" +
                "price=" + price +
                '}';
    }
}
