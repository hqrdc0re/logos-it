package com.company.lesson9_abstract_static.classwork;

public class Animal {
    private int weight;

    private static int numberOfAnimals = 0; // Змінна всього класу

    public Animal(int weight) {
        ++numberOfAnimals;
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public static int getNumberOfAnimals() { //Лише статичні поля та методи
        return numberOfAnimals;
    }

    public static void setNumberOfAnimals(int numberOfAnimals) {
        Animal.numberOfAnimals = numberOfAnimals;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "weight=" + weight +
                '}';
    }
}
