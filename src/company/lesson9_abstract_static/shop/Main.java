package com.company.lesson9_abstract_static.shop;

import com.company.lesson9_abstract_static.shop.product.Book;
import com.company.lesson9_abstract_static.shop.product.Laptop;
import com.company.lesson9_abstract_static.shop.product.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        BigDecimal price = new BigDecimal(205);
        int i = price.intValue();

        Product martinIden = new Book("Martin Iden", new BigDecimal(300), "Very interesting book",
                "Jack London", 350);

        Product hp25 = new Laptop("HP 25", new BigDecimal(17000), "Very powerful laptop",
                "15,6'", "HP");

        List<Product> products;

        System.out.println(martinIden);
        System.out.println(hp25);

        System.out.println(martinIden.getId());
        System.out.println(hp25.getId());

        Shop shop = new Shop();

//        shop.getProducts().add(martinIden);

        shop.addProduct(martinIden);
        shop.addProduct(hp25);

        System.out.println(shop.getProducts());

    }
}
