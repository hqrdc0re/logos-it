package com.company.lesson9_abstract_static.shop.product;

import java.math.BigDecimal;

public class Book extends Product{

    private String author;
    private int numberOfPages;

    public Book(String name, BigDecimal price, String description, String author, int numberOfPages) {
        super(name, price, description);
        this.author = author;
        this.numberOfPages = numberOfPages;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + getName() + '\'' +
                ", author='" + author + '\'' +
                ", numberOfPages=" + numberOfPages +
                '}';
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }
}
