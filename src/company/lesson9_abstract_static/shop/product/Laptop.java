package com.company.lesson9_abstract_static.shop.product;

import java.math.BigDecimal;

public class Laptop extends Product {

    private String screenSize;
    private String brand;

    public Laptop(String name, BigDecimal price, String description, String screenSize, String brand) {
        super(name, price, description);
        this.screenSize = screenSize;
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "screenSize='" + screenSize + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }

    public String getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(String screenSize) {
        this.screenSize = screenSize;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
