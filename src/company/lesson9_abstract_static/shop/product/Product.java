package com.company.lesson9_abstract_static.shop.product;

import java.math.BigDecimal;

public abstract class Product {

    private long id;
    private String name;
    private BigDecimal price;
    private String description;

    private static long numberOfProducts = 0;

    public Product(String name, BigDecimal price, String description) {
        ++numberOfProducts;
        this.id = numberOfProducts;
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
