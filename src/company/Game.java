package com.company;

import java.util.Scanner;

public class Game {

    int playerHP;
    String playerName;
    String playerWeapon;
    int choice;
    int monsterHP;
    int silverRing;

    public static void main(String[] args) {

        Game battle;
        battle = new Game();

        battle.playerSetUp();
        battle.townGate();
        battle.crossRoad();

        System.out.println("You are walking in the forest");

    }


    public void playerSetUp() {

        playerHP = 100;
        monsterHP = 140;

        playerWeapon = "Knife";

        System.out.println("Your HP is " + playerHP);
        System.out.println("Your weapon is " + playerWeapon);

        Scanner scanner;
        scanner = new Scanner(System.in);
        Scanner enterScanner;
        enterScanner = new Scanner(System.in);

        System.out.println("Please enter your name: ");
        playerName = scanner.nextLine();

        System.out.println("Hello " + playerName + "! Let's start the game!");
    }
    public  void townGate() {

        System.out.println("");
        System.out.println("--------------------------------------");
        System.out.println("");
//        System.out.println("\n\n\n");
        System.out.println("You are at the gate of the town");
        System.out.println("The guard is standing in front of you");
        System.out.println("What do you want to do?");
        System.out.println();
        System.out.println("1: Talk to the guard");
        System.out.println("2: Attack the guard");
        System.out.println("3: Go away");
        System.out.println("--------------------------------------");

        Scanner scanner;
        scanner = new Scanner(System.in);
        choice = scanner.nextInt();

        Scanner enterScanner;
        enterScanner = new Scanner(System.in);


        if (choice == 1) {
            if (silverRing == 1) {
                ending();
            } else {
                System.out.println("Guard: Hello there, stranger. So your name is " + playerName + "? \nSorry but we cannot let stranger enter our town.");
                enterScanner.nextLine();
                townGate();
            }
        }
         else if (choice == 2) {
                playerHP = playerHP - 10;
                System.out.println("Guard: Hey don't be stupid.\nThe guard hit you so hard and you gave up.\n(You received 10 damage)\n");
                System.out.println("Your HP is " + playerHP);
                townGate();
            }
            else if (choice == 3) {
                crossRoad();
            } else {
                townGate();
            }
        }


    public void crossRoad () {
            System.out.println("--------------------------------------");
            System.out.println("You are at a crossroad. If you go south, you will go back to the town.\n\n");
            System.out.println("1: Go north");
            System.out.println("2: Go east");
            System.out.println("3: Go south");
            System.out.println("4: Go west");
            System.out.println("--------------------------------------");

            Scanner scanner;
            scanner = new Scanner(System.in);
            choice = scanner.nextInt();

            if (choice == 1) {
                north();
            }
            if (choice == 2) {
                east();
            }
            if (choice == 3) {
                townGate();
            }
            if (choice == 4) {
                west();
            } else {
                crossRoad();
            }

        }

    public void north () {
            System.out.println("\n--------------------------------------\n");
            System.out.println("There is a river. You drink a water and rest at the riverside");
            System.out.println("Your HP is recovered");
            playerHP = playerHP + 10;
            System.out.println("Your HP is " + playerHP);
            System.out.println("\n\n1: Go back to the crossroad ");
            System.out.println("\n--------------------------------------\n");

            Scanner scanner;
            scanner = new Scanner(System.in);
            choice = scanner.nextInt();

            if (choice == 1) {
                crossRoad();
            } else {
                north();
            }
        }

    public void east () {
            System.out.println("\n--------------------------------------\n");
            System.out.println("You walked into the forest and found a Long Sword!");
            playerWeapon = "Long Sword";
            System.out.println("Your weapon is " + playerWeapon);
            System.out.println("\n\n1: Go back to the crossroad");
            System.out.println("\n--------------------------------------\n");

            Scanner scanner;
            scanner = new Scanner(System.in);
            choice = scanner.nextInt();

            if (choice == 1) {
                crossRoad();
            } else {
                north();
            }

        }

    public void west () {
            System.out.println("\n--------------------------------------\n");
            System.out.println("You encounter a Monster!\n");
            System.out.println("1: Fight!");
            System.out.println("2: Run!");
            System.out.println("\n--------------------------------------\n");

            Scanner scanner;
            scanner = new Scanner(System.in);
            choice = scanner.nextInt();

            if (choice == 1) {
                fight();
            }
            if (choice == 2) {
                crossRoad();
            } else {
                west();
            }
        }
    public void fight () {
            System.out.println("\n--------------------------------------\n");
            System.out.println("You HP is " + playerHP);
            System.out.println("Monster's HP is " + monsterHP);
            System.out.println("\n1: Attack!");
            System.out.println("2: Run!");
            System.out.println("\n--------------------------------------\n");

            Scanner scanner;
            scanner = new Scanner(System.in);
            choice = scanner.nextInt();

            if (choice == 1) {
                attack();
            }
            if (choice == 2) {
                crossRoad();
            } else {
                fight();
            }

        }
    public void attack () {
            int playerDamage = 0;

            if (playerWeapon.equals("Knife")) {
                playerDamage = new java.util.Random().nextInt(40);
            }
            if (playerWeapon.equals("Long Sword")) {
                playerDamage = new java.util.Random().nextInt(60);
            }
            System.out.println("You attacked the monster and gave " + playerDamage + "damage");

            monsterHP = monsterHP - playerDamage;
            System.out.println("Monster HP is " + monsterHP);

            if (monsterHP < 1) {
                win();
            }
            if (monsterHP > 0) {
                int monsterDamage = 0;

                monsterDamage = new java.util.Random().nextInt(30);

                System.out.println("The monster attacked you and gave " + monsterDamage + "damage!");

                playerHP = playerHP - monsterDamage;

                System.out.println("Your HP is " + playerHP);

                if (playerHP < 1) {
                    dead();
                } else if (playerHP > 0) {
                    fight();
                }
            }
        }
    public void dead () {
            System.out.println("\n--------------------------------------\n");
            System.out.println("You are dead!");
            System.out.println("\n\nGAME OVER !");
            System.out.println("\n--------------------------------------\n");
        }
    public void win () {
            System.out.println("\n--------------------------------------\n");
            System.out.println("You killed the monster!");
            System.out.println("The monster drooped a ring");
            System.out.println("You obtain a silver ring!\n\n");
            System.out.println("1: Go east");
            System.out.println("\n--------------------------------------\n");

            silverRing = 1;

            Scanner scanner;
            scanner = new Scanner(System.in);
            choice = scanner.nextInt();

            if (choice == 1) {
                crossRoad();
            } else {
                win();
            }

        }
    public void ending () {
            System.out.println("\n------------------------------------------------------------------\n");
            System.out.println("Guard: Oh you killed that goblin!?? Great!");
            System.out.println("Guard: It seems you are a trustworthy guy. Welcome to our town!");
            System.out.println("\n\n       THE END          ");
            System.out.println("\n------------------------------------------------------------------\n");
   }
}
