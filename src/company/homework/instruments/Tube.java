package company.homework.instruments;

public class Tube implements Instrument{
    int d;

    public Tube(int d) {
        this.d = d;
    }

    @Override
    public void play() {
        System.out.println("Грає труба " + d);
    }
}
