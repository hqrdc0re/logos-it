package company.homework.instruments;

public interface Instrument {
    String KEY = "До мажор";

    public void play();
}
