package company.homework.instruments;

import java.util.ArrayList;
import java.util.List;

public class Main implements Instrument{
    public static void main(String[] args) {
        Guitar guitar = new Guitar(2);
        Tube tube = new Tube(20);
        Drums drums = new Drums(9);

        List<Instrument>type = new ArrayList<>();

        type.add(guitar);
        type.add(tube);
        type.add(drums);

        for(Instrument thing : type ){
            thing.play();
        }
    }

    @Override
    public void play() {
        return ;
    }
}
