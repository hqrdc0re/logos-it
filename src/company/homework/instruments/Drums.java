package company.homework.instruments;

public class Drums implements Instrument{
     int size;

    public Drums(int size) {
        this.size = size;
    }

    @Override
    public void play() {
        System.out.println("Грають барабани " + size);
    }
}
