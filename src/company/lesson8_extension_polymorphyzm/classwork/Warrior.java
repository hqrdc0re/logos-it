package com.company.lesson8_extension_polymorphyzm.classwork;

public class Warrior extends Unit {

    private String orden;

    public Warrior() {
    }

    public Warrior(int damage, int armor, String orden) {
        super(damage, armor);
        this.orden = orden;
    }

    public void hello() {
        System.out.println("Hello");
    }

    @Override
    public void attack() {
        super.attack();
        System.out.println("Warrior attack enemy and deal " + getDamage()*2 + " damage.");
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    @Override
    public String toString() {
        return "Warrior{" +
                "orden='" + orden + '\'' +
                '}';
    }
}
