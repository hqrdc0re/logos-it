package com.company.lesson8_extension_polymorphyzm.classwork;

public class Archer extends Unit {

    @Override
    public void attack() {
        System.out.println("Archer attack enemy and deal " + getDamage() + " damage.");
    }

    public void hello() {
        System.out.println("Hello");
    }
}
