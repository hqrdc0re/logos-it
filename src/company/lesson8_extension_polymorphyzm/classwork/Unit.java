package com.company.lesson8_extension_polymorphyzm.classwork;

public class Unit {

    private int damage;
    private int armor;

    public Unit() {
    }

    public Unit(int damage, int armor) {
        this.damage = damage;
        this.armor = armor;
    }

    public void attack() {
        System.out.println("Unit attacks enemy and deals " + damage + " damage.");
    }

    public void greeting() {
        System.out.println("Hello");
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    @Override
    public String toString() {
        return "Unit{" +
                "damage=" + damage +
                ", armor=" + armor +
                '}';
    }
}
