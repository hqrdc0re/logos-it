package com.company.lesson6_wrappers_git;

import java.util.ArrayList;
import java.util.List;

public class HW_6_MykolaPaslavskyi {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList();

        list.add(10);
        list.add(15);
        list.add(1);
        list.add(6);
        list.add(11);

        for(int i = 0; i < list.size(); ++i) {
            if (list.get(i) % 2 != 0) {
                list.remove(i);
                --i;
            }
        }

        System.out.println(list);
    }
}
